[English](#markdown-header-input-validation-user-registration) | [Español](#markdown-header-validacion-de-entrada-registracion-de-usuario)

# Validación de entrada - Registración de usuario

![keyboard.jpg](images/keyboard.jpg)
![input_validation.png](images/input_validation.png)

##Objetivos:

1. Aprender la importancia de validar toda información entrada por un usuario
2. Estar consciente de diferentes vulnerabilidades y de posibles ataques

La validación de entradas es escencial en el desarrollo de aplicaciones seguras ya que es la primera línea de defensa de todas las aplicaciones.  La gran mayoría de las vulnerabilidades en aplicaciones es debido a pobre validación de las entradas. Ejemplo de vulnerabilidades explotadas por pobre validación de entradas lo son:

1. Desbordamiento del Búfer 
2. Cross Site Scripting
3. Inyección de SQL, XML u otros lenguajes
4. Recorrido de directorios
5. Acceso a archivos privados

Los atacantes utilizan errores en la validación de entradas para acceder a información privilegiada, para acceder a los sistemas, o para causar negación de servicios.
En este laboratorio, vas a practicar a validar entradas hecha por un usuario que está creando una cuenta.

Ten en mente:

* **Verificar el largo:** variables son verificadas para asegurar que son del tamaño apropiado. Por ejemplo, un número de teléfono de Puerto Rico tiene 10 dígitos.
* **Verificar el formato:** Verificar que la data está en un formato específico. Por ejemplo, las fechas se pueden escribir con el formato DD/MM/YYYY.
* **Tipo de variable:** Vas a usar C++ para este programa; recuerda usar los métodos. Por ejemplo, puedes usar `str.length()` para obtener el largo de una cadena de caracteres.

Vas a estar trabajando con el archivo `UserValidation.cpp`. Nota que la función `main` ya esta completada. Su tarea es implementar las funciones que se necesitan para validar lo que el usuario inserta como entrada.

##Ejercicio 1

Para esta parte vas a implementar la función `bool validateUsername(const string &username)`. Esta función recibe lo que entró el usuario como nombre de usuario.

**Instrucciones**

Validar que el nombre de usuario es:

1. Solo tiene dígitos (usa `isdigit()` para verificar si el carácter es un dígito)
2. El usuario puede insertar el nombre de usuario con guiones entre medio. Por ejemplo, 123-45-6789.
3. Al terminar de limpiar la entrada, verifica que solo tenga 9 caracteres de largo.

##Ejercicio 2

Para esta parte vas a implementar la función `bool validatePassword(const string &password)`. Esta función recibe lo que el usuario entró como contraseña.

**Instrucciones**

Validar que la contraseña es:

1. 8 o más caracteres de largo
2. Por lo menos 1 letra mayúscula (Usa `isupper()` para verificar si es mayúscula)
3. Por lo menos 1 letra minúscula (Usa `islower()` para verificar si es minúscula)
4. Al menos un símbolo
5. Al menos un dígito

Cuando termines, abre el terminal y corre `make` para compilar. Usa `./UV` para correr el programa.


---


# Input validation - User registration

![keyboard.jpg](images/keyboard.jpg)
![input_validation.png](images/input_validation.png)

##Objectives:

1. Learn the importance of input validation
2. Be aware of different vulnerabilities and possible attacks

Input validation is esential in the development of secure applications because it is the first line of defense of every application.  The vast majority of vulnerabilities in applications is due to poor input validation of the applications.  Example of vulnerabilities explited by poor input validation are:

1. Buffer overflows
2. Cross Site Scripting
3. SQL, XML or other languages injection
4. Directory Traversal
5. Access to private files

The attackers use errors in input validation to gain access to priviledged information, to gain access to systems, or to cause denial of services.
In this lab, you will learn to validate all input inserted by an user who is creating an account. 

Keep in mind:

* **Length check:** variables are checked to ensure they are the appropriate length, for example, a US telephone number has 10 digits.
* **Format check:** Checks that the data is in a specified format (template), e.g., dates have to be in the format DD/MM/YYYY.
* **Variable type:** You will use C++ for this program; remember the use of methods. For example, you can use `str.length()` to obtain the length of a string.

You will be working with the file `UserValidation.cpp`. Note that the main function is already completed. Your task is to implement the functions that are needed to validate the user's input.

##Exercise 1

For this part you will need to implement the function `bool validateUsername(const string &username)`. This function receives what the user inserted as the username.

**Instructions**

Validate that the username is:

1. Only has digits (use `isdigit()` to verify if the character is a digit)
2. The user can insert the username with dashes in between. For example, 123-45-6789.
3. When finished cleaning the input, verify that it only has 9 digits.

 
##Exercise 2

 For this part you will need to implement the function `bool validatePassword(const string &password)`. This function receives what the user inserted as the password.

 **Instructions**

Validate that the password is:

1. 8 or more characters of length
2. At least 1 uppercase letter (Use `isupper()` to verify if the character is uppercase)
3. At least 1 lowercase letter (Use `islower()` to verify if the character is lowercase)
4. Al least 1 symbol
5. At least 1 digit

After it is done, open the terminal and run `make` to compile. Use `./UV` to run the program.

---

##References:

 [1] http://cis1.towson.edu/~cssecinj/modules/cs0/input-validation-cs0-c/

 [2] http://www.mrmartincurrie.com/tag/input-validation/

 [3] http://www.howtogeek.com/174952/the-20-most-important-keyboard-shortcuts-for-windows-pcs/